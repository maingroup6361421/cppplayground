#include <iostream>

#include "pointer_to_member/pointer_to_member.h"

int main([[maybe_unused]] int argc, [[maybe_unused]] char** argv)
{
    std::cerr << "Running test...\n";

    pointer_to_member::foo();

    return 0;
}
