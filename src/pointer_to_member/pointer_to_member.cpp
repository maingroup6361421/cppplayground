#include "pointer_to_member/pointer_to_member.h"

namespace pointer_to_member {

struct Foo {
    int first_field;
    int second;
};

int& get_field(Foo& c, int Foo::* field)
{
    return c.*field;
}

void foo()
{
    Foo foo {.first_field = 10, .second = 99};
    get_field(foo, &Foo::first_field) = 100;
}
} // namespace pointer_to_member
